void
got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{

	static int count = 1;

	int s;	// socket
	const int on = 1;
	const struct ip *iph;              /* The IP header */
	const struct icmp *icmph;            /* The ICMP header */
	struct sockaddr_in dst;

	int size_ip;


	iph = (struct ip*)(packet + SIZE_ETHERNET);
	size_ip = iph->ip_hl*4;	// size of ip header

	if (iph->ip_p != IPPROTO_ICMP || size_ip < 20) {
		return;
	}

	icmph = (struct icmp*)(packet + SIZE_ETHERNET + size_ip);

	printf("[%d] ICMP packet sent from %s\n", count, inet_ntoa(iph->ip_src) );

	char buf[htons(iph->ip_len)];
	struct spoof_packet *spoof = (struct spoof_packet *) buf;

	memcpy(buf, iph, htons(iph->ip_len));

	(spoof->iph).ip_src = iph->ip_dst;
	(spoof->iph).ip_dst = iph->ip_src;

	(spoof->iph).ip_sum = 0;

	(spoof->icmph).icmp_type = ICMP_ECHOREPLY;
	// always set code to 0
	(spoof->icmph).icmp_code = 0;

	(spoof->icmph).icmp_cksum = 0;
	(spoof->icmph).icmp_cksum =
        in_cksum((unsigned short *) &(spoof->icmph), sizeof(spoof->icmph));

	printf("Spoofed ICMP reply packet src : %s\n",inet_ntoa((spoof->iph).ip_src));
	printf("Spoofed ICMP reply packet dst : %s\n\n",inet_ntoa((spoof->iph).ip_dst));

	memset(&dst, 0, sizeof(dst));
    dst.sin_family = AF_INET;
    dst.sin_addr.s_addr = (spoof->iph).ip_dst.s_addr;

	/* create RAW socket */
	if((s = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        printf("socket() error");
		return;
	}

	/* socket options, tell the kernel we provide the IP structure */
	if(setsockopt(s, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on)) < 0) {
		printf("setsockopt() for IP_HDRINCL error");
		return;
	}

	if(sendto(s, buf, sizeof(buf), 0, (struct sockaddr *) &dst, sizeof(dst)) < 0) {
		printf("sendto() error");
	}

	close(s);

	count++;
return;
}
